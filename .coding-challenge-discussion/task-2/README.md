# Task 2 (Stock symbol typeahead)

**Problem Statement**

```
Business requirement: As a user I should be able to type into
the symbol field and make a valid time-frame selection so that
the graph is refreshed automatically without needing to click a button.
```

_**Make a PR from the branch `feat_stock_typeahead` to `master` and provide a code review on this PR**_

> Add comments to the PR. Focus on all items that you can see - this is a hypothetical example but let's treat it as a critical application. Then present these changes as another commit on the PR.

## Discussion of solution

For this task I made a couple of choices that I think warrant explicit discussion so as to elucidate my thought process:

As can be seen from the minimal code changes I made, I did not endeavor to embellish my solution with details that *in my opinion* are missing from the stated business requirement.

  - I did not add logic to improve the user experience when data is in the process of fetching (such as a loading spinner / placeholder content)
  - I did not add logic to improve the UX when there is an error fetching the data (either because the API failed or because the user input an invalid symbol or some other such error)
  - I did not remove the button that manually invokes the data retrieval
  - I did not add logic to more reliably handle the http errors that might occur in the *effects* code

In a "real" environment, the expectations around these behaviors and changes would be items that I would clarify with business stakeholders prior to (or during) development rather than taking a guess (however educated it might be) about the expected user experience. What I had to go off of was the stated business requirement of "we don't want to have to click search to fetch new data" and the simple changes I made are sufficient for this end.

Taken in isolation, I think it is a reasonable first-blush reaction to question if what I changed here is "enough" (and that's why I felt the need to write this discussion :] ), but when this requirement is taken in context of the existing and current behavior of this application, which is effectively a "prototype" level of polish, I think it is reasonable not to apply my own personal interpretation of "what the user would want" as gold-plating on top of the stated business requirement. This is the same rationale I applied when I opted not to change anything whatsoever about styling of this application, even though I am quite confident this does not look nearly as nice as we might expect "in the end".

To reiterate, I acknowledge the eventual need to add more error handling (in the http calls, in the *effect* callbacks, in the user interface, in the user experience, etc). In a real scenario, I would absolutely pursue answers to these unknown from business stakeholders. In this coding challenge task, I felt that for me to take a guess at "what they *meant*" when they wrote the business requirement would be equally likely to be correct as it would be to create technical debt code that needed to be cleaned up once requirements were clarified, not to mention taking me longer to write.