# Task 4 (Proxy server with cache)

**Problem Statement**

```
Technical requirement: the server `stocks-api` should be used as a proxy
to make calls. Calls should be cached in memory to avoid querying for the
same data. If a query is not in cache we should call-through to the API.
```

_**Implement the solution and make a PR from the branch `feat_proxy_server` to `master`**_

> It is important to get the implementation working before trying to organize and clean it up.

## Discussion of solution

For this task I opted to use the in-built caching mechanisms from `hapi` which are simplistic but also require very little setup or boilerplate. Implementing this proxy server also provided the opportunity to remove the API key secrets from the client code (at the cost that now there is a dependency on the proxy server being up and running in order to do `e2e` tests). I introduced *configuration* object to house the runtime secrets and fixed data (such as the server port) which enables the use of environment variables to configure the runtime, including replacing the API key.

One additional item to point out is that the solution here uses *only proxy API* caching, so while we will not incur direct financial-cost/round-trip-time from the third party data vendor, we will still incur some time cost for by hitting our proxy. One way this might be addressed would be to introduce an http cache *in the client*, either in addition to or instead of the cache in the proxy server. I have implemented such a cache in a personal Angular project before, so I have decided to crib from that code of mine to add here for the sake of this discussion about what such a client-side cache might look like/how it would be used.

**The HTTP Cache Interceptor**

```typescript
import { Injectable } from '@angular/core';
import { HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorConstants } from '../../constants/behavior.constants';
import moment, { Moment } from 'moment';
import { tap } from 'rxjs/operators';
import { U } from '../static-utility/static-utilities.service';

@Injectable()
export class CacheInterceptor implements HttpInterceptor {
  private cache = new Map<string, { urlWithParams: string; expiration: Moment; response: HttpResponse<any> }>();

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    const CACHE_ENTRY = this.cache.get(req.url);
    let skipCache = true;

    if (U.isNotEmpty(CACHE_ENTRY)) {
      if (moment().isSameOrBefore(CACHE_ENTRY.expiration) && req.urlWithParams === CACHE_ENTRY.urlWithParams) {
        skipCache = false;
      } else {
        this.cache.delete(req.url);
      }
    }

    return U.isBooleanFalse(skipCache)
      ? Observable.of(CACHE_ENTRY.response)
      : next.handle(req).pipe(
          tap((httpResponse) => {
            if (httpResponse instanceof HttpResponse) {
              const CACHE_TTL_HEADER = req.headers.get(BehaviorConstants.HTTP_CLIENT_CACHE_TTL_HEADER);

              if (U.isNonBlankString(CACHE_TTL_HEADER)) {
                this.cache.set(req.url, {
                  urlWithParams: req.urlWithParams,
                  expiration: moment().add(+CACHE_TTL_HEADER, 'milliseconds'),
                  response: httpResponse,
                });
              } else {
                // All GET requests are cached for a small period unless overridden by the caching ttl header
                if (req.method === 'GET') {
                  this.cache.set(req.url, {
                    urlWithParams: req.urlWithParams,
                    expiration: moment().add(BehaviorConstants.DEFAULT_HTTP_CLIENT_CACHE_TTL, 'milliseconds'),
                    response: httpResponse,
                  });
                }
              }
            }
          })
        );
  }
}

```

**(Abbreviated) Usage in the *Interceptors* array**

```typescript
import { NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { CacheInterceptor } from '../providers/intercept/cache.interceptor';

@NgModule({
  declarations: [...],
  imports: [
    ...
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: CacheInterceptor, multi: true },
    ...
  ],
})
export class AppModule {}

```

This provides a mechanism to encapsulate the HTTP Caching within the client directly and prevent *ever sending a request to the proxy if we already have the data*. One drawback here is a larger memory footprint for the client, but it does provide an alternative mechanism for caching these data requests.