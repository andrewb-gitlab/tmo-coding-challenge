# Task 1 (Code review of `master` and cleanup)

**Problem Statement**

> Please provide a short code review of the base `master` branch:
> 
> 1. What is done well?
> 2. What would you change?
> 3. Are there any code smells or problematic implementations?
> 
> Make a PR to fix at least one of the issues that you identify

## Code review of `master`

> 1. What is done well?

1. The Nx workspace brings nice organization, structure, defaults, and tooling to the picture. This manifests in several nice things out of the box such as:
   - Monorepo structure that reduces the cognitive burden of sharing code and managing intricate dependencies - we all work off the same versions of code and version control is the source of truth rather than a blend of vcs + public npm + private npm + who-knows-what-else.
   - Code splitting through the use of modules (which facilitates lazy loading modules which is good for everyone, users and us)
   - Good default tooling that is aware of the "whole world" (i.e. cypress and jest, prettier, tslint, editorconfig - all get to be configured once and benefit from being in the same repo)
   - High re-usability of code, especially app-agnostic components and typescript definitions
   - Etc - we don't need to hash through everything listed in the Nx website :)
2. There appears to be nice work done in separating concerns (through Nx methodologies) into modules that can be shared. The big win here is that the applications are very simple architecturally since they primarily compose together behavior from `libs`.
3. The inclusion of NgRx means that we get the benefits of redux with our state which are many - from better reliability and single source of truth to a developer experience that is unmatched via the redux dev tools (and time travel).
4. It seems that most conventions about how to split and name code are being followed re: NgRx which makes it easier for someone new to the project to know where to expect things to live.
5. I am a fan of the inclusion of the `.vscode` directory and the `extensions.json` file - I'm a big fan of **vscode** and think this type of declarative approach to normalizing developer workspaces is very helpful for collaboration, pair programming, and troubleshooting. There are other **vscode** features I will bring into scope throughout this set of exercises to help work around a handful of the higher priority issues I identified during code review.
6. Inclusion of the `package-lock.json` in version control is a good call as it helps to ensure that environments can share the exact same code - unfortunately this is not the default for npm (it is for yarn), but having this in here does at least open the possibility of pinning resolved versions through the `npm ci` install method.



> 2. What would you change? / 3. Are there any code smells or problematic implementations?

For the sake of simpler organization,  I have combined numbers 2 & 3 from the problem statement into a single section of this review as both would contain items that could or should be changed/addressed. Instead of the provided delineation, I've broken things up as "Higher" and "Lower" priority where the former are items that should probably be prioritized and addressed/worked-around and the latter are items that would be good to get to, but are not necessarily deal breaking in my opinion. Some items I've explicitly marked [**Optional**] to indicate

> I thought about this while looking at the code, but it's not something that needs to be changed/addressed with immediate urgency.



#### Higher priority

1. There is application behavior that does not currently work as currently written, namely the `chart` does not render after fetching ticket data.

2. After install (or upon running `npm audit`) there are many `audit` issues identified by npm, including a ***critical*** vulnerability found in a third-party dependency (plus thousands of *high* severity results and a large number of moderate and low level issues)

   ![npm audit results](./npm-warnings.png)

   As of now, the one ***critical*** audit result is a `Prototype Pollution` vulnerability (potentially allowing arbitrary code execution on the server) in the transitive **handlebars** dependency (used by **jest**) which is only needed in the dev/test realms and is not required in production - that said, there may be items among the ***high*** severity results that impact production code and as such time should be allocated to review this report and begin the process of upgrading dependencies to newer versions where these issues are resolved.

   **Note** - the process of upgrading dependencies might have far reaching implications for shared code, organizational security/compliance, other teams, etc. This process should be approached deliberately and with thorough vetting.

3. Currently there are several tests (both unit/spec tests and e2e tests) that do not pass.

4. Separate from the failing tests, there is also plenty of opportunity here to expand on the current suite of tests to improve coverage and our "refactor safety net".

5. `app.module.ts` - Unless there is a very good reason to include it, the import of `StoreDevtoolsModule.instrument({...` should probably only take place when the environment is not production. This is extra code that the user likely does not need shipped to them in the production bundle.

6. `chart.component.ts` (and more) - There are observables that are being subscribed to but never unsubscribed from which will result in memory leaks. These observables (such as `data$` in this component) need to be unsubscribed in the `OnDestroy` lifecycle hook to prevent memory leaks.

7. `chart.component.ts` - I think it would make more sense for this component to be "dumber" and not worry about how it gets the data it should render. This could be accomplished a few ways:

   - Change the `@Input()` from taking an `Observable` to taking the actual data array
   - Remove the input property and instead inject a `service` that marshals the data from the external source
   - Remove the input property and instead inject the `store` to extract the data from the NgRx datastore

   In the context of this component and how it is likely to be used as a shared component, it probably makes the most sense to simply take the data as an input property (instead of injecting either a service or store segment) allowing this "presentational" component to be "what data I am displaying" agnostic. A follow on to this would be to parametrize the chart properties (`chart.type`, `chart.columnNames`, etc) as additional input properties, making this component essentially an adapter/abstraction to the underlying chart library (Google Chart as of now) - this would (should) make it easier to swap in a different library easier if needed.

8. `price-query-transformer.util.ts` - There are two items I noticed with this file:
   
   - Depending on business requirements, we may need to make the `date-fns` usage "timezone aware" (as of now, it is not clear exactly what zone these datetimes will be in). In general, stock price tracking seems pretty time-sensitive, and having incorrect zone information may or may not be an issue...also time and timezones are *just the worst* :)
- This might take a bit of digging (I started that by looking at https://github.com/webpack/webpack/issues/1750), but it's not clear if the `ES Module` version of `lodash` (imports from `lodash-es`) can be reliably tree-shaken by webpack, and if not usage of the couple of lodash methods in this file might result in the inclusion of all of lodash in the compiled bundle - that could be a lot more weight than is worth it for just these two methods.
  
9. `price-query.reducer.ts` - This is (potentially) a point of clarification, but it's not clear if the implementation of the **PriceQueryActionTypes.PriceQueryFetched** case in `priceQueryReducer` will return a ***new*** object for the `state` key or if it will return a reference to the existing value - I think this bears doing a bit more digging than I have ability to do in the midst of this review, but if my suspicion is correct that this will return a reference to the existing state then this is potentially disastrous to our ability to ensure that only reducers can change state values, thus potentially losing much of the benefit of the `store`. It's also possible that the `EntityAdapter#addAll` method will implicitly do this for us under the hood, but we should dig a bit to confirm.

10. `price-query.effects.ts` - During construction of the url for stock information, we need to `url encode` the variables to ensure they don't break the http query. Additionally, it *might* make sense to pull the logic of *how* to perform this effect (i.e. making an http call to one API or another) out into an injectable service that can be used elsewhere (including within this effect code).

11. `price-query.facade.ts` - It is unclear why there is a `skip(1)` in the observable pipe here for `priceQueries$`. Unless I'm missing something about why it is needed, it seems that at best this will cause us to miss a message from the Subject which could mean we're missing data we could otherwise be using. This would come into play *for sure* if we were to use a `BehaviorSubject` for this observable to, for instance, provide default values to the facade.

12. In my opinion ***all*** dependency versions should be pinned at all times and only incremented deliberately. This, in my experience, is the only way to ensure that all environments (dev / test / stage / prod) stay in sync and are definitely running the same code. For that reason, I think the `package.json` should use explicit versions and not `semver` calculated version notation.

13. [**Optional**] Due to the use of an older version of Angular (and the associated build tooling), we are unable to support the latest (stable) version of `node`, i.e. `node 12.x.x` on the Linux platform (and possibly other platforms as well). This is because the build tooling requires the `node-gyp` binary for compilation of **scss/sass** and there is no precompiled binary publicly available and the required headers are not available to the system when `npm install` attempts to compile the binary locally. While Linux may be the minority platform for local development, it is highly likely that ***ci/cd*** takes place on a Linux platform.

    ![node-gyp errors](./linux-node-12-build-error.png)

    **Note** - I think this falls into the "higher priority" section as it is pretty well tied to upgrading dependencies and the fact that it could add a lot of pain to introducing **scss/sass** if we decided to do that for styling.

14. [**Optional**] I think we should work towards removing `any` type declarations as it prevents the TS compiler from providing any typing assistance. In addition to this, we should work towards extracting (potentially sharable) interfaces/types for all data members (for example in `libs/shared/ui/chart/src/lib/chart/chart.component.ts` there is a member `chart` which could have a more well defined type interface rather than an inline declaration).

#### Lower priority

1. There are several "tidy up" opportunities such as:
   - Unifying naming strategy for symbols project-wide (consts vs enums vs classes vs locals, etc)
   - Removing unused imports, unused component html/style files, unused method params and DI injected items
   - Sorting imports in a consistent way (node modules, then nx libs, then relative imports, and alphabetical from there)
   - Removing unused `lifecycle` methods (such as the empty `OnInit` methods scaffolded by default when generating with the Angular cli)
3. `price-query.actions.ts` - The naming conventions suggested by the **NgRx** documentation for Actions is useful in debugging (and within the redux devtools) to identify originator and description information at a glance when Actions fire in the store. I would recommend updating these Actions to have names of the format `[Action Originator] action description`.
4. `[Stocks] main.ts` - It would be good to do something more than log the error caught by the application bootstrap (for instance, sending this error off to a tracking endpoint somewhere). Additionally, it is uncommon to wrap the application bootstrap inside of a **DOMContentLoaded** callback - it is not wrong and will absolutely work, but it is not required as the default injection point for the webpack build is just before the closing `</body>` tag which implies the DOM will already be loaded and parsed enough for the application to bootstrap without issue.
5. `price-query.effects.ts` - Depending on how fault tolerant we want the fetching of data to be, it might make sense to either add retry logic to this http request observable pipe, or better yet, to create a general purpose `HttpRetryInterceptor` that can add **retry** and **backoff** logic to multiple (or all) API requests.
6. `price-query.type.ts` - There are a couple of minor points on these definitions:
   - The interface definitions could probably be moved into a library as these seem likely to be repeated by an API definition.
   - The use of `dateNumeric` in the **PriceQuery** definition feels like a leaking implementation detail. As this is a derived value, it might make sense to simply derive it at the point of usage, or to rename this field to something less "implementation-y" - for instance it could be called ***epoch***, as this implies a numeric value and has the added bonus of being shorter and less similar to the existing ***date*** field.
7. `price-query.reducer.ts` - The function `sortByDateNumeric` is a generically useful "numeric value" comparator for a `sort` function but the name provides the implication that it's only useful for dates. I think it should be renamed to reflect its more general usability of numerically sorting values (rather than the default of lexicographical).
8. `chart.component.ts` - Depending on the method of refactor chosen to clean up the `@Input()` (discussed earlier), the change detection strategy of this component could potentially be changed to something less watchful since the data should be coming only from the parent component and won't be changing from within the scope of this component.
9. `stocks.component.ts` - Looking towards a future where there is a server proxying requests, it makes sense to extract an interface for the objects in the `timePeriods` member that can be shared with the API when validating requests for price information. In addition to the shape of these objects, there should probably also be an `enum` for the specific text values of the "value" field since these are essentially magic string values unseen within UI.
10. [**Optional**] `stocks.component.ts` - There are other validators that probably make sense for the fields in this form builder. It would at least make sense to include a *minimum length* validator, though this may not be true for all stock exchanges.
11. [**Optional**] It is "typical" to put routing definitions into a separate module that is then imported into the application/component module, but this is not mandatory.
12. [**Optional**] Application styling seems lacking and less than user friendly, though this may be at the "prototype" stage and as such there may be no requirements around UI/UX - which is to say this isn't "wrong" at all but does warrant pointing out :)



## Items addressed from Code Review

The following are items from the `code review` above for which I've implemented fixes/changes or in some way addressed:

"**Higher Priority**"

- `#13` - This was a higher priority for me on my local machine than I think it would be more generally as I run the latest stable-channel Node (12.16.x) on Linux for my personal machine so I was unable to get through install of the current dependencies without error. To address this I've introduced the contents of the `.devcontainer` directory. This is a set of files that essentially containerize the whole development environment to run on an ecosystem that likely matches what was used by the creator of this repository back in April 2019 (in particular it's an Ubuntu distribution with the latest-stable-at-the-time version of Node, 10.19.0).

  To get the most out of this containerization you must use `vscode` as your editor, but even without vscode the `Dockerfile` and `docker-compose.yml` files are useful on their own with any other editor - you just get a lot more by leveraging these files with the vscode `Remote Development` extensions. What this enables is running the editor GUI on your host machine but executing all code, tools, intellisense, etc within the context of your container and its filesystem. This means you can build and run your code in an ecosystem *identical* to what might be used in **ci/cd** and **production** even for local development. Additionally it lets us declaratively define what the development environment needs to consist of, and turns on-boarding of a new developer into a turnkey operation - very cool!

- `#1` - Implemented a *minimal-scope* change to get the chart data rendering again which entailed adding API keys to the `environment.ts` files (**Note** that this is *not* the optimal place to have these API keys! They will be moved in later tasks, so this is ok for now, but in general secrets should not live in client code!) and modifying the `*ngIf` structural directive on the `Google Chart` (**Note** that this did not address the poor type checking for the relevant fields nor desired refactors indicated later in the code review - this is intentionally a minimal set of changes just to get this feature working again).

- `#3` - Implemented basic fixes for all broken tests. This entailed adding missing imports, adding missing application code (working with the assumption that the *tests* were correct and the *app* was what was wrong and not the other way around), and stubbing some dependencies for injection tokens. These updates are not bullet proof, but they are enough to get back to a green build which is mandatory for having a solid wall to our back when we go to implement new tests / application features. This is a start, and coverage needs to be improved from here.

- `#6` & `#7` - Refactored the `chart` to act more as a presentational component abstraction on top of the Google Chart component. This meant adding more `@Input()` properties and removing the data interface that required an observable to be passed in. In removing the observable, the memory leak was also solved.

**"Lower Priority"**

- `#1` - Dealt with much of the low hanging fruit to tidy up formatting and remove unused code - largely used the built-in `nx` task for running `prettier` in write mode.
- `#5` - Added these interface declarations to the exports included in the `lib` **index.ts** which automatically makes them visible to shared code thanks to `Nx`. Also renamed the interface field to `epoch` to address the other part of the comment.



**Note** - there were several of these code review items that were also good candidates to address for the sake of Task 1 and I chose a handful of them to prioritize as they were ones that I suspected would be most helpful in subsequent tasks. I could spend a good deal more time addressing every item I raised here, but I will leave the rest as an exercise for another time!