# Task 3 (Custom date range)

**Problem Statement**

```
Business requirement: As a user I want to choose custom dates
so that I can view the trends within a specific period of time.
```

_**Implement this feature and make a PR from the branch `feat_custom_dates` to `master`.**_

> Use the material date-picker component

> We need two date-pickers: "from" and "to". The date-pickers should not allow selection of dates after the current day. "to" cannot be before "from" (selecting an invalid range should make both dates the same value)

## Discussion of solution

This task was relatively straight forward despite that it required making some updates in several places. I wanted the filtration to happen high up in the call chain and part of this also included attempting to fetch as little data source API as possible (while still capturing as much as needed to fulfill the client side filtering). For this reason, changes were required in many aspects of the NgRx logic, but all of them are fairly procedural. One point to note is that in implementing this logic, I wanted to extract the "calculate the minimum required API time range parameter" logic into a testable (in isolation) place, and in extracting this, I went ahead and addressed more of the code review comments from Task 1. Overall this task yielded a pretty flexible solution and added a good deal of test coverage for behavior of the application logic.