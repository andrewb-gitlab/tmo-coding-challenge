import { Component, Input } from '@angular/core';

@Component({
  selector: 'coding-challenge-chart',
  templateUrl: './chart.component.html'
})
export class ChartComponent {
  @Input() data: any[];
  @Input() title: '';
  @Input() type: string;
  @Input() columnNames: string[] = [];
  @Input() options: any = {};
}
