import { parse } from 'date-fns';
import { map, pick } from 'lodash-es';
import {
  PriceQuery,
  PriceQueryDateRangeFilter,
  PriceQueryResponse
} from './price-query.type';

export function transformPriceQueryResponse(
  response: PriceQueryResponse[]
): PriceQuery[] {
  return map(
    response,
    responseItem =>
      ({
        ...pick(responseItem, [
          'date',
          'open',
          'high',
          'low',
          'close',
          'volume',
          'change',
          'changePercent',
          'label',
          'changeOverTime'
        ]),
        epoch: parse(responseItem.date).getTime()
      } as PriceQuery)
  );
}

export function filterPriceQueryByDate(
  priceQueries: PriceQuery[],
  filter: PriceQueryDateRangeFilter
): PriceQuery[] {
  return priceQueries.filter(query => {
    const QUERY_DATE = new Date(query.date);

    return QUERY_DATE >= filter.start && QUERY_DATE <= filter.end;
  });
}
