import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { map, skip } from 'rxjs/operators';
import { FetchPriceQuery } from './price-query.actions';
import { PriceQueryPartialState } from './price-query.reducer';
import { getAllPriceQueries, getSelectedSymbol } from './price-query.selectors';
import { PriceQueryDateRangeFilter } from './price-query.type';

@Injectable()
export class PriceQueryFacade {
  selectedSymbol$ = this.store.pipe(select(getSelectedSymbol));
  priceQueries$ = this.store.pipe(
    select(getAllPriceQueries),
    skip(1),
    map(priceQueries =>
      priceQueries.map(priceQuery => [priceQuery.date, priceQuery.close])
    )
  );

  constructor(private store: Store<PriceQueryPartialState>) {}

  fetchQuote(
    symbol: string,
    period: string,
    filter: PriceQueryDateRangeFilter
  ) {
    this.store.dispatch(new FetchPriceQuery(symbol, period, filter));
  }
}
