import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import {
  filterPriceQueryByDate,
  transformPriceQueryResponse
} from './price-query-transformer.util';
import { PriceQueryAction, PriceQueryActionTypes } from './price-query.actions';
import { PriceQuery } from './price-query.type';

export const PRICEQUERY_FEATURE_KEY = 'priceQuery';

export interface PriceQueryState extends EntityState<PriceQuery> {
  selectedSymbol: string;
}

export function sortByDateNumeric(a: PriceQuery, b: PriceQuery): number {
  return a.epoch - b.epoch;
}

export const priceQueryAdapter: EntityAdapter<PriceQuery> = createEntityAdapter<
  PriceQuery
>({
  selectId: (priceQuery: PriceQuery) => priceQuery.epoch,
  sortComparer: sortByDateNumeric
});

export interface PriceQueryPartialState {
  readonly [PRICEQUERY_FEATURE_KEY]: PriceQueryState;
}

export const initialState: PriceQueryState = priceQueryAdapter.getInitialState({
  selectedSymbol: ''
});

export function priceQueryReducer(
  state: PriceQueryState = initialState,
  action: PriceQueryAction
): PriceQueryState {
  switch (action.type) {
    case PriceQueryActionTypes.PriceQueryFetched: {
      return priceQueryAdapter.addAll(
        filterPriceQueryByDate(
          transformPriceQueryResponse(action.queryResults),
          { start: action.filter.start, end: action.filter.end }
        ),
        state
      );
    }
    case PriceQueryActionTypes.SelectSymbol: {
      return {
        ...state,
        selectedSymbol: action.symbol
      };
    }
  }
  return state;
}
