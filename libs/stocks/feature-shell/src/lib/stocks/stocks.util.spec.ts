import { subMonths, subYears } from 'date-fns';
import { timePeriodForDateFilterStart, TimePeriodValue } from './stocks.util';

describe('StocksUtil#timePeriodForDateFilterStart', () => {
  it('should should calculate max when date is missing', () => {
    expect(timePeriodForDateFilterStart(null)).toBe(TimePeriodValue.max);
  });

  it('should should calculate max when date is more than 5 years ago', () => {
    expect(timePeriodForDateFilterStart(subYears(Date.now(), 6))).toBe(
      TimePeriodValue.max
    );
  });

  it('should should calculate 5y when date is between 2 and 5 years ago', () => {
    expect(timePeriodForDateFilterStart(subYears(Date.now(), 4))).toBe(
      TimePeriodValue.fiveYears
    );
  });

  it('should should calculate 2y when date is between 1 and 3 years ago', () => {
    expect(timePeriodForDateFilterStart(subYears(Date.now(), 2))).toBe(
      TimePeriodValue.twoYears
    );
  });

  it('should should calculate 6m when date is between 3 and 6 months ago', () => {
    expect([
      TimePeriodValue.sixMonths,
      TimePeriodValue.yearToDate
    ]).toContainEqual(timePeriodForDateFilterStart(subMonths(Date.now(), 5)));
  });

  it('should should calculate 3m when date is between 1 and 3 months ago', () => {
    expect([
      TimePeriodValue.threeMonths,
      TimePeriodValue.yearToDate
    ]).toContainEqual(timePeriodForDateFilterStart(subMonths(Date.now(), 2)));
  });

  it('should should calculate 1m when date is between now and 1 month ago', () => {
    expect([
      TimePeriodValue.oneMonth,
      TimePeriodValue.yearToDate
    ]).toContainEqual(timePeriodForDateFilterStart(subMonths(Date.now(), 0)));
  });
});
