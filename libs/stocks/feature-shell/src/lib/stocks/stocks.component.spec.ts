import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import {
  MatButtonModule,
  MatDatepickerModule,
  MatFormFieldModule,
  MatInputModule,
  MatNativeDateModule,
  MatSelectModule
} from '@angular/material';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { SharedUiChartModule } from '@coding-challenge/shared/ui/chart';
import {
  PriceQueryDateRangeFilter,
  PriceQueryFacade
} from '@coding-challenge/stocks/data-access-price-query';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { StocksComponent } from './stocks.component';
import { TimePeriodValue } from './stocks.util';

const MOCK_FACADE = {
  priceQueries$: of([['2020', 2020]]),
  fetchQuote: (
    symbol: string,
    period: string,
    filter: PriceQueryDateRangeFilter
  ) => {
    return { symbol, period, filter };
  }
};

describe('StocksComponent', () => {
  let component: StocksComponent;
  let fixture: ComponentFixture<StocksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [StocksComponent],
      imports: [
        ReactiveFormsModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatButtonModule,
        MatDatepickerModule,
        MatNativeDateModule,
        SharedUiChartModule,
        NoopAnimationsModule
      ],
      providers: [
        {
          provide: PriceQueryFacade,
          useValue: MOCK_FACADE
        },
        { provide: Store, useValue: {} }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StocksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('stockPickerForm', () => {
    beforeEach(() => {
      component.stockPickerForm.reset();
    });

    it('should be marked valid when filled with valid fields', () => {
      const NOW = new Date();

      component.stockPickerForm.get('symbol').setValue('AAPL');
      component.stockPickerForm
        .get('period')
        .setValue(TimePeriodValue.oneMonth);
      component.stockPickerForm.get('startDate').setValue(NOW);
      component.stockPickerForm.get('endDate').setValue(NOW);

      expect(component.stockPickerForm.valid).toBeTruthy();
    });

    it('should be marked invalid when unfilled', () => {
      expect(component.stockPickerForm.valid).toBeFalsy();
    });
  });

  describe('onStartDateChange', () => {
    beforeEach(() => {
      component.stockPickerForm.reset();
    });

    it('should set the end date and date period automatically when only the start date is set', () => {
      const NOW = new Date();

      component.stockPickerForm.controls.startDate.setValue(NOW);
      component.onStartDateChange();

      expect(component.stockPickerForm.controls.endDate.value).toBeDefined();
      expect(component.stockPickerForm.controls.period.value).toBeDefined();
    });

    it('should not modify the end date if the start date is not set', () => {
      component.onStartDateChange();

      expect(component.stockPickerForm.controls.endDate.value).toBeNull();
    });
  });
});
