export type TimePeriod = {
  label: string;
  value: string;
  maxDaysAgo: number;
};
