import { differenceInDays, startOfYear } from 'date-fns';
import { TimePeriod } from './stocks.type';

const MAX_DAYS_IN_MONTH = 31;
const MAX_DAYS_IN_YEAR = 366;

export enum TimePeriodValue {
  max = 'max',
  fiveYears = '5y',
  twoYears = '2y',
  oneYear = '1y',
  yearToDate = 'ytd',
  sixMonths = '6m',
  threeMonths = '3m',
  oneMonth = '1m'
}

export const ALL_TIME_PERIODS: TimePeriod[] = [
  {
    label: 'All available data',
    value: TimePeriodValue.max,
    maxDaysAgo: Number.MAX_SAFE_INTEGER
  },
  {
    label: 'Five years',
    value: TimePeriodValue.fiveYears,
    maxDaysAgo: MAX_DAYS_IN_YEAR * 5
  },
  {
    label: 'Two years',
    value: TimePeriodValue.twoYears,
    maxDaysAgo: MAX_DAYS_IN_YEAR * 2
  },
  {
    label: 'One year',
    value: TimePeriodValue.oneYear,
    maxDaysAgo: MAX_DAYS_IN_YEAR
  },
  {
    label: 'Year-to-date',
    value: TimePeriodValue.yearToDate,
    maxDaysAgo: differenceInDays(new Date(), startOfYear(new Date()))
  },
  {
    label: 'Six months',
    value: TimePeriodValue.sixMonths,
    maxDaysAgo: MAX_DAYS_IN_MONTH * 6
  },
  {
    label: 'Three months',
    value: TimePeriodValue.threeMonths,
    maxDaysAgo: MAX_DAYS_IN_MONTH * 3
  },
  {
    label: 'One month',
    value: TimePeriodValue.oneMonth,
    maxDaysAgo: MAX_DAYS_IN_MONTH
  }
];

export function timePeriodForDateFilterStart(date: Date): string {
  if (!date) {
    return TimePeriodValue.max;
  }

  const DAYS_AGO = differenceInDays(new Date(), date);

  return ALL_TIME_PERIODS.sort((a, b) => a.maxDaysAgo - b.maxDaysAgo).find(
    tp => DAYS_AGO < tp.maxDaysAgo
  ).value;
}
