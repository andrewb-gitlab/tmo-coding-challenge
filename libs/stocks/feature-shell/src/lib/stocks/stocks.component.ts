import { Component, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
  PriceQueryDateRangeFilter,
  PriceQueryFacade
} from '@coding-challenge/stocks/data-access-price-query';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, takeUntil } from 'rxjs/operators';
import { ALL_TIME_PERIODS, timePeriodForDateFilterStart } from './stocks.util';

@Component({
  selector: 'coding-challenge-stocks',
  templateUrl: './stocks.component.html',
  styleUrls: ['./stocks.component.css']
})
export class StocksComponent implements OnDestroy {
  private destroying$ = new Subject<void>();

  today = new Date();
  stockPickerForm: FormGroup;
  quotes$ = this.priceQuery.priceQueries$;
  timePeriods = ALL_TIME_PERIODS;

  constructor(private fb: FormBuilder, private priceQuery: PriceQueryFacade) {
    this.quotes$ = this.priceQuery.priceQueries$;
    this.buildFormAndValueSubscribers();
  }

  fetchQuote() {
    if (this.stockPickerForm.valid) {
      const { symbol, period, startDate } = this.stockPickerForm.value;
      const filter: PriceQueryDateRangeFilter = {
        start: startDate,
        end: this.today
      };
      const END_DATE_FIELD = this.stockPickerForm.get('endDate');

      if (END_DATE_FIELD && END_DATE_FIELD.value) {
        filter.end = END_DATE_FIELD.value;
      }

      this.priceQuery.fetchQuote(symbol, period, filter);
    }
  }

  onStartDateChange(): void {
    const START_DATE_FIELD = this.stockPickerForm.get('startDate');
    const END_DATE_FIELD = this.stockPickerForm.get('endDate');

    if (START_DATE_FIELD && START_DATE_FIELD.value && START_DATE_FIELD.valid) {
      this.stockPickerForm
        .get('period')
        .setValue(
          timePeriodForDateFilterStart(
            this.stockPickerForm.get('startDate').value
          )
        );

      END_DATE_FIELD.setValue(this.today);
      END_DATE_FIELD.enable();
    } else {
      END_DATE_FIELD.setValue(null);
      END_DATE_FIELD.disable();
    }
  }

  ngOnDestroy() {
    this.destroying$.next();
    this.destroying$.complete();
  }

  private buildFormAndValueSubscribers() {
    this.stockPickerForm = this.fb.group({
      symbol: [null, Validators.required],
      period: [null, Validators.required],
      startDate: [null],
      endDate: [{ value: null, disabled: true }]
    });

    ['symbol', 'period', 'startDate', 'endDate'].forEach(fbControl => {
      this.stockPickerForm
        .get(fbControl)
        .valueChanges.pipe(
          takeUntil(this.destroying$),
          debounceTime(300),
          distinctUntilChanged()
        )
        .subscribe(
          () => {
            this.fetchQuote();
          },
          e => console.error(e)
        );
    });
  }
}
