import { StocksAppConfigToken } from './stocks-app-data-token.constant';

describe('StocksAppDataTokenConstants', () => {
  it('should have exported constants', () => {
    expect(StocksAppConfigToken).toBeDefined();
  });
});
