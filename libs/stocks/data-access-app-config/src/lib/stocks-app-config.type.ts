export type StocksAppConfig = {
  production: boolean;
  apiURL: string;
};
