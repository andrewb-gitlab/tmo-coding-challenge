/**
 * This is not a production server yet!
 * This is only a minimal backend to get started.
 **/
import { TimePeriodValue } from '@coding-challenge/stocks/feature-shell';
import { Server } from 'hapi';
import { StockApiEnvironment } from './app/environment/stock-api-environment';
import { stockPriceQuery } from './app/stocks-server/stocks-server.methods';

const ENVIRONMENT = StockApiEnvironment.fromProcess(process);

const init = async () => {
  const server: Server = new Server({
    port: ENVIRONMENT.serverPort(),
    host: ENVIRONMENT.serverHost(),
    routes: {
      cors: true
    }
  });

  server.route({
    method: 'GET',
    path: '/api/stocks',
    handler: async ({ query }) => {
      const symbol = query.symbol;
      const period: TimePeriodValue = query.period as TimePeriodValue;
      const url = `${ENVIRONMENT.iexCloudBasePath()}/beta/stock/${symbol}/chart/${period}?token=${ENVIRONMENT.iexCloudToken()}`;

      return server.methods.stockPriceQuery(url);
    }
  });

  server.method('stockPriceQuery', stockPriceQuery, {
    cache: {
      expiresIn: 1_000 * 60 * ENVIRONMENT.cacheTimeoutMinutes(),
      generateTimeout: 1_000 * 5
    }
  });

  await server.start();

  console.log('Server running on %s', server.info.uri);
};

process.on('unhandledRejection', err => {
  console.log(err);
  process.exit(1);
});

init();
