import { StockApiEnvironment } from './stock-api-environment';

describe('StockApiEnvironment', () => {
  it('should create', () => {
    expect(new StockApiEnvironment({})).toBeDefined();
  });

  it('should have default configuration properties', () => {
    const ENV = new StockApiEnvironment({});

    expect(ENV.cacheTimeoutMinutes()).toBe(60);
    expect(ENV.iexCloudBasePath()).toBeTruthy();
    expect(ENV.serverHost()).toBe('localhost');
    expect(ENV.serverPort()).toBe(3333);
  });
});
