export class StockApiEnvironment {
  readonly env: NodeJS.ProcessEnv;

  constructor(systemEnv: NodeJS.ProcessEnv) {
    this.env = systemEnv;
  }

  static fromProcess(nodeProcess: NodeJS.Process) {
    return new StockApiEnvironment(nodeProcess.env);
  }

  iexCloudBasePath() {
    return this.env['IEXCLOUD_API_BASE'] || 'https://sandbox.iexapis.com';
  }

  iexCloudToken() {
    // Note that the token here is provided for the sake of
    // the coding challenge and should normally be replaced with
    // an empty string
    return (
      this.env['IEXCLOUD_API_TOKEN'] || 'Tpk_eefd5583069e4c27b69feec7ad7a08a0'
    );
  }

  serverHost(): string {
    return this.env['SERVER_HOST'] || 'localhost';
  }

  serverPort(): number {
    return Number(this.env['SERVER_PORT']) || 3333;
  }

  cacheTimeoutMinutes(): number {
    return Number(this.env['CACHE_TIMEOUT_MINUTES']) || 60;
  }
}
