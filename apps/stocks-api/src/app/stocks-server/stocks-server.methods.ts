import axios from 'axios';

export async function stockPriceQuery(url: string) {
  try {
    const { data } = await axios.get(url);

    return data;
  } catch (e) {
    throw new Error('Error fetching data!');
  }
}
