import { StocksAppConfig } from '@coding-challenge/stocks/data-access-app-config';

export const environment: StocksAppConfig = {
  production: true,
  apiURL: 'http://localhost:4200/api'
};
